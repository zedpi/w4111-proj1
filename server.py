#!/usr/bin/env python2.7

"""
Columbia's COMS W4111.001 Introduction to Databases
Example Webserver

To run locally:

    python server.py

Go to http://localhost:8111 in your browser.

A debugger such as "pdb" may be helpful for debugging.
Read about it online.
"""

import os
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from flask import Flask, request, render_template, g, redirect, Response

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)


#
# The following is a dummy URI that does not connect to a valid database. You will need to modify it to connect to your Part 2 database in order to use the data.
#
# XXX: The URI should be in the format of: 
#
#     postgresql://USER:PASSWORD@104.196.18.7/w4111
#
# For example, if you had username biliris and password foobar, then the following line would be:
#
#     DATABASEURI = "postgresql://biliris:foobar@104.196.18.7/w4111"
#
DATABASEURI = "postgresql://jz2816:4111@35.231.44.137/proj1part2"


#
# This line creates a database engine that knows how to connect to the URI above.
#
engine = create_engine(DATABASEURI)


@app.before_request
def before_request():
  """
  This function is run at the beginning of every web request 
  (every time you enter an address in the web browser).
  We use it to setup a database connection that can be used throughout the request.

  The variable g is globally accessible.
  """
  try:
    g.conn = engine.connect()
  except:
    print ("uh oh, problem connecting to database")
    import traceback; traceback.print_exc()
    g.conn = None

@app.route('/')
def main():
    return render_template("main.html")

@app.route('/district_search')
def district_search():
    return render_template("district_search.html")

@app.route('/filter_search')
def filter_search():
    return render_template("filter_search.html")

@app.route('/optimize_search')
def optimize_search():
    return render_template("optimize_search.html")

@app.route('/district_search/query', methods=['GET'])
def query1():
  print (request.args)
  name = request.args.getlist('name')
  view = "SELECT * FROM WiFi_in_" + str(name[0])
  try:
      cursor = g.conn.execute(view)
      entries = []
      for row in cursor.fetchall():
          temp = "Wifi ID: " + str(row[0]) + ", " + "address: " + str(row[1]) + ", " + "cost: " + str(row[2]) + ", " + "number of likes: " + str(row[3]) + ", " + "accessible from: " + str(row[4])
          entries.append(temp)
      cursor.close()
      if entries:
          return render_template('query.html', entries=entries)
      else:
          return render_template('empty.html')
  except:
      return render_template('empty.html')

@app.route('/filter_search/query', methods=['GET'])
def query2():
  print (request.args)
  name = request.args.getlist('name')
  criterion = request.args.getlist('filter')
  view = "SELECT * FROM " + criterion[0] + "_WiFi_in_" + str(name[0])
  try:
      cursor = g.conn.execute(view)
      entries = []
      for row in cursor.fetchall():
          temp = "Wifi ID: " + str(row[0]) + ", " + "address: " + str(row[1]) + ", " + "cost: " + str(row[2]) + ", " + "number of likes: " + str(row[3]) + ", " + "accessible from: " + str(row[4])
          entries.append(temp)
      cursor.close()
      if entries:
          return render_template('query.html', entries=entries)
      else:
          return render_template('empty.html')
  except:
      return render_template('empty.html')

@app.route('/optimize_search/query', methods=['GET'])
def query3():
  print (request.args)
  name = request.args.getlist('name')
  view = "SELECT * FROM Recommended_WiFi_in_" + str(name[0])
  try:
      cursor = g.conn.execute(view)
      entries = []
      for row in cursor.fetchall():
          temp = "Wifi ID: " + str(row[0]) + ", " + "address: " + str(row[1]) + ", " + "borough: " + str(row[2]) + ", " + "cost: " + str(row[3]) + ", " + "number of likes: " + str(row[4]) + ", " + "accessible from: " + str(row[5])
          entries.append(temp)
      cursor.close()
      if cursor:
          return render_template('query.html', entries=entries)
      else:
          return render_template('empty.html')
  except:
      return render_template('empty.html')


@app.teardown_request
def teardown_request(exception):
  try:
    g.conn.close()
  except Exception as e:
    pass


if __name__ == "__main__":
  import click

  @click.command()
  @click.option('--debug', is_flag=True)
  @click.option('--threaded', is_flag=True)
  @click.argument('HOST', default='0.0.0.0')
  @click.argument('PORT', default=8111, type=int)
  def run(debug, threaded, host, port):
    HOST, PORT = host, port
    print ("running on %s:%d" % (HOST, PORT))
    app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


  run()
